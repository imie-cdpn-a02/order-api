GET|HEAD  | api/orders          
 Liste toutes les commandes

POST      | api/orders          
Passer une nouvelle commande. Json dans le body
```
{
	"client_id": 12,
	"reference":"deza32186",
	"adress":"deza32186",
	"price":128,
	"menu" : [
		{
			"menu_id": 1,
			"quantity": 1
		},
			{
			"menu_id": 2,
			"quantity": 12
		}
	]
}
```
DELETE    | api/orders/{order}  
Parametre route id (integer)


GET|HEAD  | api/orders/{order}  
Parametre route id (integer)

PUT|PATCH | api/orders/{order} 
Parametre route id (integer) + parametres du bodu voir méthod store