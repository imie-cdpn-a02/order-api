<?php

use App\Models\MenuOrder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<=20; $i++) {

            \App\Models\Order::create([
                'ref_client' => rand(10,9999),
                'reference' => str_random(16),
                'adress' => str_random(50),
                'price' => random_int(10,999),
                'status' => 'pending'
            ]);


        }

        $id = 1;
        for ($i=0; $i<=20; $i++) {
            \App\Models\OrderMenu::create([
                'order_id' => $id,
                'ref_menu' => rand(1, 50),
                'quantity' => rand(1,10),
            ]);
            $t = rand(0, 1);
            if ($t === 1) {
                \App\Models\OrderMenu::create([
                    'order_id' => $id,
                    'ref_menu' => rand(1, 50),
                    'quantity' => rand(1,10),
                ]);
            }
            $id++;
        }
    }
}
