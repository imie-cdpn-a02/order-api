<?php

namespace App\Http\Controllers;

use App\Models\MenuOrder;
use App\Models\Order;
use App\Models\OrderMenu;
use Illuminate\Http\Request;

class OrderController extends Controller {

    public function index(){
        $orders =  Order::with('menuOrders')->get();
        return $orders;
    }

    public function show(Order $order){
        return $order->menuOrders;
    }


    public function store(Request $request){
        $menu = $request->get('menu');
        $data = $request->except(['menu']);

        $data['status'] = 'pending';


        $result = Order::create($data);

        foreach ($menu as $m) {
            OrderMenu::create([
                'order_id' => $result->id,
                'menu_id' => $m['menu_id'],
                'quantity' => $m['quantity']
            ]);
        }

        return response()->json();
    }

    public function update(Order $order, Request $request){

        $menu = $request->get('menu');
        $data = $request->except(['menu']);

        $data['status'] = 'pending';


        $result = $order::update($data);

        foreach ($menu as $m) {
            OrderMenu::create([
                'order_id' => $result->id,
                'menu_id' => $m['menu_id'],
                'quantity' => $m['quantity']
            ]);
        }

        return response()->json();

    }


    public function destroy(Order $order){
        $order->delete();
        return response()->json();
    }


}
