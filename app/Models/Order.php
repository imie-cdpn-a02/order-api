<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $fillable = [
        'ref_client',
        'reference',
        'ref_menu',
        'quantity',
        'adress',
        'price',
        'status',
        'delivrary_at'
    ];

    public function menuOrders(){
        return $this->hasMany(OrderMenu::class);
    }
}
