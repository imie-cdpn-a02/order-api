<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderMenu extends Model {

    protected $table = 'order_menu';

    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'menu_id',
        'quantity'
    ];


}
